<?php require_once './conf/config.php'; ?>
<!DOCTYPE html>
<html lang="fr">
    <?php include_once 'head.inc.php'; ?>

    <body>

        <div class="container">
            <?php include_once 'comptable.menu.inc.php'; ?>
            <br>
            <br>
            <br>
            <?php
            $lesVisiteurs = Visiteur::fetchAllByEtat('EC');
            ?> 
            <!-- Liste des fiches de frais en cours -->

            <?php
            foreach ($lesVisiteurs as $visiteur) :
                $ficheFrais = $visiteur->getFicheFraisByEtat('EC');
                ?>
                <div class="row">
                    <div class="col-lg-offset-1 col-lg-10">
                        <div class="panel panel-default">

                            <!-- HEAD PANEL -->
                            <div class="panel-heading">
                                <h3 class="panel-title "> 
                                    <p class="text-uppercase">
                                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>

                                        <?php
                                        echo $visiteur->getPrenom() . " " . $visiteur->getNom();
                                        ?>
                                        <br>
                                        PERIODE : 
                                        <?php echo $ficheFrais->getMoisAnnee(); ?><br>ETAT : <?php
                                        echo $ficheFrais->getEtat()->getLibelleEtat();
                                        ?>
                                    </p>
                                </h3>

                            </div>

                            <!-- BODY PANEL -->
                            <div class="panel-body">

                                <form method="post" action="comptable.traitement.modificationFicheFrais.php">
                                    <input type="hidden" name="idFicheFrais" value="<?php echo $ficheFrais->getIdFicheFrais(); ?>" />
                                    <?php
                                    $collectionLigneFraisForfait = $ficheFrais->getCollectionLigneFraisForfait();
                                    foreach ($collectionLigneFraisForfait as $ligneFraisForfait):
                                        ?>
                                        <!-- FRAIS FORFAIT PANEL -->
                                        <div class="col-sm-4">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">
                                                        <?php
                                                        echo $ligneFraisForfait->getFraisForfait()->getLibelleFraisForfait();
                                                        ?>
                                                    </h3>
                                                </div>
                                                <div class="panel-body">
                                                    <p>
                                                        <input type="number" id="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
                                                               name="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
                                                               <?php if ($ligneFraisForfait->getFraisForfait()->getIdFraisForfait() == "NUI"): ?>
                                                                   min="0" max="31"
                                                               <?php endif; ?>
                                                               pattern="[0-9]+"

                                                               value="<?php
                                                               echo $ligneFraisForfait->getQuantite();
                                                               ?>"
                                                               />
                                                    </p>
                                                    <p>
                                                        <?php //echo $fraisForfaitCourant->montantFicheFrais;  ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                    ?>
                                    <p>
                                        <button type="submit" class="btn btn-warning col-lg-2 col-lg-offset-9">Modifier</button>
                                    </p>
                                </form>                               


                                <?php
                                $ligneFraisHorsForfait = $ficheFrais->getCollectionLigneFraisHorsForfait();
                                if ($ligneFraisHorsForfait != null):
                                    ?>
                                    <div class="col-lg-8">
                                        <p class="text-uppercase">
                                            frais hors forfait
                                        </p>
                                        <!-- FRAIS HORS FORFAIT -->
                                        <table class="table table-striped">
                                            <th>Libellé</th>
                                            <th>Date</th>
                                            <th>Montant</th>
                                            <th></th>
                                            <?php
                                            foreach ($ligneFraisHorsForfait as $ligneFraisHorsForfait):
                                                ?>
                                                <tr class="warning">
                                                    <td><?php echo $ligneFraisHorsForfait->getLibelle(); ?></td>
                                                    <td><?php echo $ligneFraisHorsForfait->getDate()->format('d-m-Y'); ?></td>
                                                    <td><?php echo $ligneFraisHorsForfait->getMontant(); ?></td>
                                                    <th></th>
                                                    <td>
                                                        <a href="comptable.traitement.suppressionLigneFraisHorsForfait.php?idLigneFraisHorsForfait=<?php echo $ligneFraisHorsForfait->getIdLigneFraisHorsForfait(); ?>">
                                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                        </a>
                                                    </td>
                                                </tr>


                                                <?php
                                            endforeach;
                                            ?>
                                        </table>
                                    </div><!-- FIN SECTION TAB -->

                                    <?php
                                endif;
                                ?>

                                <a class="btn btn-success col-md-3 col-lg-offset-5 " href="comptable.traitement.validationFicheFrais.php?idFicheFrais=<?php echo $ficheFrais->getIdFicheFrais(); ?>" role="button">VALIDER LA FICHE DE FRAIS</a>          


                            </div><!-- FIN BODY PANEL -->








                        </div><!-- FIN PANEL GENERAL -->
                    </div><!-- FIN DIMENSION ROW -->
                </div><!-- FIN ROW -->
            <?php endforeach; ?>


        </div>

        <div class="container">
            <?php include_once 'comptable.menu.inc.php'; ?>
            <br>
            <br>
            <br>
            <?php
            $lesVisiteurs = Visiteur::fetchAllByEtat('VA');
            ?> 
            <!-- Liste des fiches de frais en cours -->

            <?php
            foreach ($lesVisiteurs as $visiteur) :
                $ficheFrais = $visiteur->getFicheFraisByEtat('VA');
                ?>
                <div class="row">
                    <div class="col-lg-offset-1 col-lg-10">
                        <div class="panel panel-default">

                            <!-- HEAD PANEL -->
                            <div class="panel-heading">
                                <h3 class="panel-title "> 
                                    <p class="text-uppercase">
                                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>

                                        <?php
                                        echo $visiteur->getPrenom() . " " . $visiteur->getNom();
                                        ?>
                                        <br>
                                        PERIODE : 
                                        <?php echo $ficheFrais->getMoisAnnee(); ?><br>ETAT : <?php
                                        echo $ficheFrais->getEtat()->getLibelleEtat();
                                        ?>
                                    </p>
                                </h3>

                            </div>

                            <!-- BODY PANEL -->
                            <div class="panel-body">

                                <form method="post" action="comptable.traitement.modificationFicheFrais.php">
                                    <input type="hidden" name="idFicheFrais" value="<?php echo $ficheFrais->getIdFicheFrais(); ?>" />
                                    <?php
                                    $collectionLigneFraisForfait = $ficheFrais->getCollectionLigneFraisForfait();
                                    foreach ($collectionLigneFraisForfait as $ligneFraisForfait):
                                        ?>
                                        <!-- FRAIS FORFAIT PANEL -->
                                        <div class="col-sm-4">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">
                                                        <?php
                                                        echo $ligneFraisForfait->getFraisForfait()->getLibelleFraisForfait();
                                                        ?>
                                                    </h3>
                                                </div>
                                                <div class="panel-body">
                                                    <p>
                                                        <input type="number" id="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
                                                               name="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
                                                               <?php if ($ligneFraisForfait->getFraisForfait()->getIdFraisForfait() == "NUI"): ?>
                                                                   min="0" max="31"
                                                               <?php endif; ?>
                                                               pattern="[0-9]+"

                                                               value="<?php
                                                               echo $ligneFraisForfait->getQuantite();
                                                               ?>"
                                                               />
                                                    </p>
                                                    <p>
                                                        <?php //echo $fraisForfaitCourant->montantFicheFrais;  ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                    ?>
                                    <p>
                                        <button type="submit" class="btn btn-warning col-lg-2 col-lg-offset-9">Modifier</button>
                                    </p>
                                </form>                               


                                <?php
                                $ligneFraisHorsForfait = $ficheFrais->getCollectionLigneFraisHorsForfait();
                                if ($ligneFraisHorsForfait != null):
                                    ?>
                                    <div class="col-lg-8">
                                        <p class="text-uppercase">
                                            frais hors forfait
                                        </p>
                                        <!-- FRAIS HORS FORFAIT -->
                                        <table class="table table-striped">
                                            <th>Libellé</th>
                                            <th>Date</th>
                                            <th>Montant</th>
                                            <th></th>
                                            <?php
                                            foreach ($ligneFraisHorsForfait as $ligneFraisHorsForfait):
                                                ?>
                                                <tr class="warning">
                                                    <td><?php echo $ligneFraisHorsForfait->getLibelle(); ?></td>
                                                    <td><?php echo $ligneFraisHorsForfait->getDate()->format('d-m-Y'); ?></td>
                                                    <td><?php echo $ligneFraisHorsForfait->getMontant(); ?></td>
                                                    <th></th>
                                                    <td>
                                                        <a href="comptable.traitement.suppressionLigneFraisHorsForfait.php?idLigneFraisHorsForfait=<?php echo $ligneFraisHorsForfait->getIdLigneFraisHorsForfait(); ?>">
                                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                        </a>
                                                    </td>
                                                </tr>


                                                <?php
                                            endforeach;
                                            ?>
                                        </table>
                                    </div><!-- FIN SECTION TAB -->

                                    <?php
                                endif;
                                ?>         


                            </div><!-- FIN BODY PANEL -->








                        </div><!-- FIN PANEL GENERAL -->
                    </div><!-- FIN DIMENSION ROW -->
                </div><!-- FIN ROW -->
            <?php endforeach; ?>


        </div>



        <!-- Site footer -->
        <footer class="footer">
            <p>&copy; GSB 2015</p>
        </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>


