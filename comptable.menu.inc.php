<!-- The justified navigation menu is meant for single line per list item.
Multiple lines will require custom code not provided by Bootstrap. -->
<div class="masthead">
    <h3 class="text-muted">GSB</h3>
    <nav>
        <ul class="nav nav-justified">
            <ul class="nav nav-justified">
                <li><a href="comptable.accueil.php">Accueil</a></li>
                <li class="active"><a href="comptable.gestionFicheFrais.php">Gestion des fiches frais</a></li>
                <li><a href="exit.php">Déconnexion</a></li>
            </ul>
        </ul>
    </nav>
</div>
