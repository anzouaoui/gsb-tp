<?php
require_once './conf/config.php';

$visiteurCourant = $_SESSION["connectedUser"];
if (isset($_SESSION["ficheFraisCourante"])) {
    $ficheFraisCourante = $_SESSION["ficheFraisCourante"];
} else {
    $moisAnnee = date("mY");
    $ficheFraisCourante = $visiteurCourant->creerFicheFrais($moisAnnee);
}


?>
<!DOCTYPE html>
<html lang="fr">
<?php include_once 'head.inc.php'; ?>

    <body>

        <div class="container">

<?php include_once 'visiteur.menu.inc.php'; ?>
            <br><br>
            <p>
            <div class="row">
                <div class="col-md-12 col-md-offset-2">
                    <h1>Frais Hors Forfait</h1>
                </div>
            </div>
        </p>
        <form method="post" action="visiteur.traitement.saisieHorsForfait.php">
            <div class="row">

                <div class="col-md-4 col-md-offset-2">
                    <div class="input-group">
                        <span class="input-group-addon spanFixed" id="basic-addon1">Libellé</span>
                        <input type="text" id="libelle" name="libelle" class="form-control" placeholder="libellé" aria-describedby="basic-addon1">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon spanFixed" id="basic-addon1">Date</span>
                        <input id="date" name="date" pattern="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d" type="text" class="form-control" placeholder="jj-mm-aaaa" aria-describedby="basic-addon1">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon spanFixed" id="basic-addon1">Montant</span>
                        <input name="montant" id="montant" type="text" required pattern="[0-9]+(\.[0-9]+)?" class="form-control" placeholder="€" aria-describedby="basic-addon1">

                    </div>
                </div>

            </div>
            <br>

            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <button type="submit" class="btn btn-primary btn-sm">Valider</button>
                </div>
            </div>


        </form>


        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <table class="table">
                    <caption>Récapitulatif des frais hors forfait</caption>
                    <thead>
                        <tr>                        
                            <th>Libellé</th>
                            <th>Date</th>
                            <th>Montant</th>

                        </tr>
                    </thead>

                    <?php
                    $collectionLigneFraisHorsForfait = $ficheFraisCourante->getCollectionLigneFraisHorsForfait();
                    if ($collectionLigneFraisHorsForfait != null):
                        
                        foreach ($collectionLigneFraisHorsForfait as $ligneFraisHorsForfait) :
                            ?>
                            <tbody>
                                <?php ?>
                                <tr>                        
                                    <td><?php echo $ligneFraisHorsForfait->getLibelle(); ?></td>
                                    <td><?php echo $ligneFraisHorsForfait->getDate()->format('d-m-Y'); ?></td>
                                    <td><?php echo $ligneFraisHorsForfait->getMontant(); ?></td>
                                    <td><a href="visiteur.traitement.suppressionLigneFraisHorsForfait.php?idFicheFrais=<?php echo $ficheFraisCourante->getIdFicheFrais();?>&idLigneFraisHorsForfait=<?php echo $ligneFraisHorsForfait->getIdLigneFraisHorsForfait(); ?>"><span title="Supprimer" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a></td>

                                </tr>  
                                <?php ?>
                            </tbody>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </table>
            </div>

        </div>


    </div>


    <!-- Site footer -->
    <footer class="footer col-md-offset-1">
        <p>&copy; GSB 2015</p>
    </footer>

</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>

